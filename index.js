//hàm bài 1
function tinhSoNguyenDuongNhoNhat() {
  //   console.log("yes");
  var num = 0;
  var result = 0;

  for (var sum = 1; sum < 10000; sum++)
    if (((num = num + sum), console.log("num: ", num), num > 10000)) {
      //   console.log("sum", sum);
      result = sum;
      console.log("result: ", result);
      break;
    }
  document.getElementById(
    "txtResult3"
  ).innerHTML = `Số nguyên dương nhỏ nhất: ${result}`;
}
//hàm bài 2
function tinhTongLuyThua() {
  //nhập x
  var inX = document.getElementById("inputX").value * 1;
  //   console.log("inX: ", inX);
  //nhập N
  var inN = document.getElementById("inputN").value * 1;
  //   console.log("inN: ", inN);
  var result = 0;
  for (var i = 1; i <= inN; i++) {
    result = result + Math.pow(inX, i);
    // console.log("result: ", result);
    document.getElementById("txtResult4").innerHTML = `Tổng:  ${result}`;
  }
}
//hàm bài 3
function tinhGT() {
  //   console.log("yes");
  var number = document.getElementById("inputN1").value * 1;
  var result = 1;
  for (var i = 1; i <= number; i++) {
    result = result * i;
    document.getElementById("txtResult5").innerHTML = `Giai thừa: ${result}`;
  }
}
//hàm bài 4
function taoDiv() {
  //   var soTheDiv = document.getElementById("nhapsotheDiv").value * 1;
  var content = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content =
        content + `<div class="bg-danger text-white p-2">Div chẵn${i}</div>`;
    } else {
      content =
        content + `<div class="bg-primary text-white p-2">Div lẻ${i}</div>`;
    }
  }
  document.getElementById("txtResult6").innerHTML = content;
}
//hàm bài 5
//hàm check số nguyên tố
function checkSoNguyenTo(n) {
  if (n < 2) return !1;
  for (var t = 2; t <= Math.sqrt(n); t++) if (n % t == 0) return !1;
  return !0;
}
//hàm in số nguyên tố
function inSoNguyenTo() {
  var nhapsoNguyenTo = document.getElementById("nhapsoNguyenTo").value * 1;
  var result = "";
  for (var i = 1; i <= nhapsoNguyenTo; i++) {
    checkSoNguyenTo(i) && (result = result + " " + i);
  }
  document.getElementById("txtResult7").innerHTML = result;
}
